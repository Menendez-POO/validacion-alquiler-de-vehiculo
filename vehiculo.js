
/*Crear una función JavaScript. Realizar la validación de datos de alquiler de vehículos 
en JavaScript: código=alfanumérico de 5 caracteres, marca=alfanumérico de 50 caracteres,
 modelo=alfanumérico de 30 caracteres, año=numérico de 4 dígitos, fecha inicial=tipo 
 date, fecha final=tipo date). La fecha de final debe ser mayor a la fecha inicial. */


    const expresionesRegularesVehiculo = {
        codigo: /^[a-zA-Z0-9]{5}$/,
        marca: /^[a-zA-Z0-9]{50}$/,
        modelo: /^[a-zA-Z0-9]{30}$/,
        anio: /^\d{4}$/,
        fechaInicial: /^\d{4}-\d{2}-\d{2}$/,
        fechaFinal: /^\d{4}-\d{2}-\d{2}$/
    };
    // validamos cada uno de nuestros campos
    
    function validarVehiculo(codigo, marca, modelo, anio, fechaInicial, fechaFinal) {
        let valido = true;
        let mensaje = "";
        if (!expresionesRegularesVehiculo.codigo.test(codigo)) {
            valido = false;
            mensaje = "El codigo debe ser alfanumerico de 5 caracteres";
        }
        if (!expresionesRegularesVehiculo.marca.test(marca)) {
            valido = false;
            mensaje = "La marca debe ser alfanumerico de 50 caracteres";
        }
        if (!expresionesRegularesVehiculo.modelo.test(modelo)) {
            valido = false;
            mensaje = "El modelo debe ser alfanumerico de 30 caracteres";
        }
        if (!expresionesRegularesVehiculo.anio.test(anio)) {
            valido = false;
            mensaje = "El año debe ser numerico de 4 dìgitos";
        }
        if (!expresionesRegularesVehiculo.fechaInicial.test(fechaInicial)) {
            valido = false;
            mensaje = "La fecha inicial debe ser tipo date";
        }
        if (!expresionesRegularesVehiculo.fechaFinal.test(fechaFinal)) {
            valido = false;
            mensaje = "La fecha final debe ser tipo date";
        }
        if (valido) {
            alert("Vehiculo ingresado correctamente");
        } else {
            alert(mensaje);
        }
    }